#include <Servo.h>

Servo coreservo;
String inString = ""; 
int val;

void setup() {
  coreservo.attach(9);
  coreservo.write(60);
  Serial.begin(9600);
  while (!Serial) {
  }
}

void loop() {
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    if (isDigit(inChar)) {
      inString += (char)inChar;
    }
    if (inChar == '\n') {
      val = inString.toInt();
      coreservo.write(val);
      inString = "";
    }
  }
}
