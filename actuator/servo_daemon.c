#include<stdio.h>      
#include<stdlib.h>     
#include<unistd.h>     
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      
#include<termios.h>    
#include<errno.h>      
#include<string.h>  
    
#define FALSE  -1  
#define TRUE   0  
    
int UART_Open(char* port)  
{      
	int fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);  
	
	if (FALSE == fd) {  
		perror("Can't Open Serial Port");  
		return(FALSE);  
	}  
	
	if(fcntl(fd, F_SETFL, 0) < 0) {  
		printf("Error: fcntl failed!\n");  
		return(FALSE);  
	}
	
	if(0 == isatty(STDIN_FILENO)) {  
		printf("Error: The standard input is not a tty\n");  
		return(FALSE);  
	}    
	
	return fd;  
}  
   
int UART_Init(int fd, int speed, int flow_ctrl, int databits, int stopbits, char parity)  
{       
	int i;  
	int speed_arr[] = {B115200, B19200, B9600, B4800, B2400, B1200, B300};  
	int name_arr[] = {115200,  19200,  9600,  4800,  2400,  1200,  300};  
           
	struct termios options;    
	
	if( tcgetattr( fd,&options)  !=  0) {  
		perror("SetupSerial 1");      
		return(FALSE);   
	}  
    
	for ( i= 0;  i < sizeof(speed_arr) / sizeof(int);  i++) {  
		if (speed == name_arr[i]) {               
			cfsetispeed(&options, speed_arr[i]);   
			cfsetospeed(&options, speed_arr[i]);    
		}  
	}       
     
    options.c_cflag |= CLOCAL;  
	
    options.c_cflag |= CREAD;  
    
    switch(flow_ctrl) {  
        case 0 : 
              options.c_cflag &= ~CRTSCTS;  
              break;           
		case 1 : 
              options.c_cflag |= CRTSCTS;  
              break;  
		case 2 :
              options.c_cflag |= IXON | IXOFF | IXANY;  
              break;  
    }  
	
    options.c_cflag &= ~CSIZE;  
	
    switch (databits) {    
		case 5:  
                options.c_cflag |= CS5;  
                break;  
		case 6:  
                options.c_cflag |= CS6;  
                break;  
		case 7:      
                options.c_cflag |= CS7;  
                break;  
		case 8:      
                options.c_cflag |= CS8;  
                break;    
		default:     
                fprintf(stderr, "Unsupported data size\n");  
                return (FALSE);   
    }  
	
    switch (parity) {    
		case 'n':  
		case 'N':   
                 options.c_cflag &= ~PARENB;   
                 options.c_iflag &= ~INPCK;      
                 break;   
		case 'o':    
		case 'O':    
                 options.c_cflag |= (PARODD | PARENB);   
                 options.c_iflag |= INPCK;               
                 break;   
		case 'e':   
		case 'E':
                 options.c_cflag |= PARENB;         
                 options.c_cflag &= ~PARODD;         
                 options.c_iflag |= INPCK;        
                 break;  
		case 's':  
		case 'S':   
                 options.c_cflag &= ~PARENB;  
                 options.c_cflag &= ~CSTOPB;  
                 break;   
        default:    
                 fprintf(stderr, "Unsupported parity\n");      
                 return (FALSE);   
    }   

    switch (stopbits) {    
		case 1:     
                 options.c_cflag &= ~CSTOPB; break;   
		case 2:     
                 options.c_cflag |= CSTOPB; break;  
		default:     
                       fprintf(stderr, "Unsupported stop bits\n");   
                       return (FALSE);  
    }  
     
	options.c_oflag &= ~OPOST;  
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  
    options.c_cc[VTIME] = 0;   
    options.c_cc[VMIN] = 1;  
     
    tcflush(fd,TCIFLUSH);  
     
    if (tcsetattr(fd,TCSANOW,&options) != 0) {  
		perror("com set error!\n");    
		return (FALSE);   
	}  
    return (TRUE);   
}  
    
int main(int argc, char **argv)  
{  
    int fd;                            
    int err;                                                     
	char *port = "/dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_5573932323735120F020-if00";
		
	daemon(1,1);
	
	fd = UART_Open(port);                        
	err = UART_Init(fd, 9600, 0, 8, 1, 'N'); 	
    
	if (FALSE == err || FALSE == fd) {
		printf("Error: cannot init port %s\n", argv[1]);
		return FALSE;
	} 
	
	while(1)
	{
		usleep(200);
	}
}  
